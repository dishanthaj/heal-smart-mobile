package lk.healsmart.healsmartmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    String  app_server_url = "http://onlinein4.com/HealSmartAPI.aspx?method=notifications.details";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl("https://www.healsmart.lk");
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setGeolocationEnabled(true);
        myWebView.setSoundEffectsEnabled(true);
        myWebView.getSettings().setAppCacheEnabled(true);
//        myWebView.clearCache(true);
//        myWebView.clearHistory();
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


//        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
//        String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN),"");

        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.FCM_PREF), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String token = sharedPreferences.getString(getString(R.string.FCM_TOKEN),"");

        System.out.println("############  token "+token);



        Map<String, String> params = new HashMap<String, String>();
        params.put("task", "notifications.details");
        params.put("session_id", token);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                app_server_url,

                new JSONObject(params),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("############################"+response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<>();
                params.put("Content-Type","application/json");
                params.put("SecurityKEY","APIHealSm#@$`12345");

                return params;
            }

        };


        MySingleton.getmInstance(MainActivity.this).addToRequestque(jsonObjReq);


    }
}
